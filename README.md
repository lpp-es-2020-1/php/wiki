# Documentação

## Linguagem PHP

### Sumário
1. Introdução
2. História
3. Características técnicas
4. Referências

### 1. Introdução

O PHP é uma linguagem de script para uso geral, é utilizada especialmente para o desenvolvimento WEB e principalmente porque pode ser embutida dentro do HTML do lado do servidor.

No PHP, o HTML é feito de forma "mesclada" com códigos que com alguma lógica embutida. Toda instrução em PHP deve ser delimitada pelas tags de início e fim <?php e ?>. O que permite fazer uma transição entre o código PHP e o código HTML. Ele é muito parecido com o JavaScript se comparado a interatividade que ambos podem gerar para o HTML, no entanto, o primeiro é executado no servidor (gerando o HTML e então enviado para o navegador), enquanto o segundo é executado do lado do cliente, diretamente no navegador.

Os Processadores PHP estão disponíveis na maioria dos servidores Web. Seu código é interpretado no servidor Web quando um documento HTML no qual ele está embutido for requisitado por um navegador. Permitindo um acesso simples aos dados de formulários HTML, o PHP fornece suporte para muitos sistemas de gerenciamento de bancos de dados. Isso o torna uma excelente linguagem para construir programas que precisam de acesso Web a bases de dados.

### 2.História

A motivação inicial do PHP era fornecer uma ferramenta para ajudar a rastrear os visitantes no site pessoal do seu criador, Rasmus Lerdorf.

Criado em 1994 por Rasmus Lerdof, a primeira versão do PHP foi um simples conjunto de binários Common Gateway Interface (CGI) escrito em linguagem de programação C. Originalmente usado para acompanhamento de visitas para seu currículo online, ele nomeou o conjunto de scripts de "Personal Home Page Tools" mais frequentemente referenciado como "PHP Tools." Ao longo do tempo, mais funcionalidades foram desejadas, e Rasmus reescreveu o PHP Tools, produzindo uma maior e rica implementação. Este novo modelo foi capaz de interações com Banco de Dados e mais, fornecendo uma estrutura no qual os usuários poderiam desenvolver simples e dinâmicas aplicações web, como um livros de visitas. Em Junho de 1995, Rasmus liberou o código fonte do PHP Tools para o público, o que permitiu que desenvolvedores usassem da forma como desejassem. Isso permitiu - e encorajou - usuários a fornecerem correções para bugs no código, e em geral, aperfeiçoá-lo.

Em Setembro do mesmo ano, Rasmus expandiu o PHP e - por um breve período - mudou o nome PHP. Agora referindo-se a ferramenta como FI, abreviação para "Forms Interpreter", a nova implementação incluiu algumas funcionalidades básicas do PHP como bem conhecemos hoje. Tinha variáveis no estilo Perl, interpretação automática de variáveis de formulários, e sintaxe HTML embutida. A sintaxe em si era muito similar com a do Perl, porém muito mais limitada, simples, e um pouco inconsistente. De fato, para embutir o código em um arquivo HTML, desenvolvedores tinham que usar comentários HTML. Embora este método não sido inteiramente bem-recebido, FI continuou a desfrutar um crescimento e aceitação como uma ferramente CGI, mas ainda não como uma linguagem. 

Contudo, isso começou a mudar já em Outubro de 1995, Rasmus liberou um completa reescrita do código. Trazendo de volta o nome PHP, estava agora (brevemente) nomeado "Personal Home Page Construction Kit" e foi o primeiro lançamento na época, sendo considerado um avançado script de interface. A linguagem foi desenvolvida para, deliberadamente, ser parecida com C, tornando-a fácil para ser adotada por desenvolvedores habituados com C, Perl e linguagens similares. Tendo sido até este momento exclusiva para sistemas UNIX e sistemas compatíveis com POSIX, o potencial para uma implementação em um Windows NT começava a ser explorada.

O código tem outra reforma completa, e em Abril de 1996, combinando os nomes dos últimos lançamentos, Rasmus introduziu o PHP/FI. Esta segunda geração da implementação começou a realmente evoluir o PHP de um conjunto de ferramentas para sua própria linguagem de programação. Ele incluía suporte embutido dos banco de dados DBM, mSQL, e Postgres95, cookies, funções de apoio definidas pelo usuário, e muito mais. Em Junho, PHP/FI ganhou o status de versão 2.0. Um interessante fato sobre isso, porém, é que existia apenas um única completa versão do PHP 2.0. Quando finalmente se tornou um status beta em Novembro, 1997, o mecanismo de análise suvbjacente já estava inteiramente reescrito.

Apesar de ter tido um curto período de desenvolvimento, ele continuava a desfrutar de uma crescente popularidade em um ainda jovem mundo web de desenvolvimento, Em 1997 e 1998, PHP/FI teve o apoio de milhares de usuários ao redor do mundo. Uma pesquisa Netcraft de Maio de 1998, indicou que cerca de 60.000 domínios relataram ter cabeçalhos contendo "PHP", indicando que o servidor de hospedagem de fato tinha o PHP instalado. Este número pode ser equiparado com aproximadamente 1% de todos os domínios da Internet da época. Apesar destes números impressionantes, o amadurecimento do PHP/FI foi condenado a limitações; enquanto haviam vários contribuintes menores, ainda era desenvolvido principalmente por uma única pessoa.

Como podemos perceber o exemplo abaixo o código do PHP/FI era feito utilizando comentários de HTML.

**Exemplo - 1 Exemplo de código PHP/FI**
```
<!--include /text/header.html-->

<!--getenv HTTP_USER_AGENT-->
<!--ifsubstr $exec_result Mozilla-->
  Hey, you are using Netscape!<p>
<!--endif-->

<!--sql database select * from table where user='$username'-->
<!--ifless $numentries 1-->
  Sorry, that record does not exist<p>
<!--endif exit-->
  Welcome <!--$user-->!<p>
  You have <!--$index:0--> credits left in your account.<p>

<!--include /text/footer.html-->
```
#### 2.1 O PHP 3

PHP 3.0 foi a primeira versão que se assemelha com o PHP como existe hoje. PHP/FI se encontrava ainda ineficiente e não tinha recursos que precisava para prover uma aplicação eCommerce que estavam desenvolvendo para um projeto da Universidade, Andi Gutmans e Zeev Suraski de Tel Aviv, Israel, começaram outra completa reescrita do interpretador em 1997. Abordando Rasmus de forma online, eles discutiram vários aspectos para a corrente implementação e redesenvolvimento do PHP. Em um esforço para melhorar a engine e iniciar a construção em cima da base de usuários existentes do PHP/FI, Andi, Rasmus, e Zeev decidiram colaborar no desenvolvimento de uma nova e independente linguagem de programação. Essa nova linguagem foi lançada com um novo nome, que removeu a impressão do limitado uso pessoal que o nome PHP/FI 2.0 tinha mantido. Foi renomeado simplesmente para 'PHP', com o significado se tornando um acrônimo recursivo - PHP: Hypertext Preprocessor.

Um dos maiores pontos fortes do PHP 3.0 foram os fortes recursos de extensibilidade. Além de fornecer a usuários finais uma interface robusta para múltiplos banco de dados, protocolos, e APIs, a facilidade de estender a sua própria linguagem atraiu dezenas de desenvolvedores que submeteram uma variedade de módulos. Indiscutivelmente esta foi a chave para o PHP 3.0 ter sido um tremendo sucesso. Outro recurso chave foi introduzido no PHP 3.0 incluindo o suporte a programação orientada a objeto e a uma mais poderosa e consistente sintaxe de linguagem.

Em junho de 1998, com muitos novos desenvolvedores ao redor do mundo unindo esforços, PHP 3.0 foi anunciado pelo novo time de desenvolvimento do PHP como o sucessor oficial para o PHP/FI 2.0. As melhorias no PHP/FI 2.0, cessaram em Novembro do ano anterior e agora foi oficialmente finalizado. Depois de nove meses de testes abertos ao público, quando o anúncio do lançamento oficial do PHP 3.0 chegou, prontamente foi instalado em 70.000 domínios em todo mundo, e já não era mais limitado ao sistemas operacionais compatíveis ao POSIX. Uma parcela relativamente pequena de domínios informaram que o PHP foi instalado em um host com servidores executando Windows 95, 98 e NT, Macintosh. E em seu pico, PHP 3.0 foi instalado em aproximadamente 10% dos servidores web da internet.

#### 2.2 O PHP 4 

No inverno de 1998, logo após o PHP 3.0 ter sido oficialmente lançado, Andi Gutmans e Zeev Suraski começaram a trabalhar em uma reescrita do core do PHP. Os objetivos do projeto eram melhorar performance das aplicações complexas, e melhorar a modularização do código base do PHP. Tais aplicações só foram possíveis pelos novos recursos e suporte para uma ampla variedades de banco de dados de terceiros e APIs do PHP 3.0, mas o PHP 3.0 não foi projetado para trabalhar com aplicações complexas de forma eficiente.

O novo motor, chamado 'Zend Engine' (composto pelos primeiros nome, Zeev e Andi), alcançou os objetivos do projeto com sucesso, e foi introduzido em meados de 1999. O PHP 4.0 baseado neste motor, e uma variedade de novos recursos adicionais, foi oficialmente lançado em Maio de 2000, quase dois anos após seu antecessor. Além da altíssima melhoria da performance nesta versão, o PHP 4.0 incluiu outros recursos chaves, tais como suporte para maioria dos servidores web, sessões HTTP, saídas de buffering, mais maneiras seguras para manipular dados de entrada de usuários e diversas novas construções de linguagem.

#### 2.3 O PHP 5 

O PHP 5 foi lançado em Julho de 2004 após um longo desenvolvimento e vários pré-lançamentos. Principalmente impulsionado pelo seu core o Zend Engine 2.0 com um novo modelo de objeto e dezenas de outros novos recursos.

O time de desenvolvimento PHP inclui dezenas de desenvolvedores, também dezenas de outros trabalhando em algo relacionado ao PHP e apoio a projetos como PEAR, PECL, documentação, infra-estrutura de rede subjacente de bem mais de uma centena de servidores web em seis dos sete continentes do mundo. Embora seja apenas uma estimativa baseada sobre estatísticas de anos anteriores, é seguro presumir que o PHP está agora instalado em dezenas, ou mesmo talvez centenas, de milhões de domínios em todo mundo.

### 3. Características Técnicas

O PHP é focado principalmente em scripts do lado do servidor, como coletar dados de formulários, gerar conteúdo de página dinâmica ou enviar e receber cookies. Além disso, existem duas áreas principais onde os scripts PHP são usados.

#### 3.1 Aplicações
##### 3.1.1 Scripts Server-Side

Os Scripts do lado do servidor é a principal função do PHP. Para esta função são necessários três itens:

* Analisador PHP (CGI ou módulo de servidor), 
* Servidor web 
* Navegador web

Você precisa executar o servidor web, com uma instalação de PHP conectada. Você pode acessar a saída do programa PHP com um navegador da web, visualizando a página PHP por meio do servidor. Todos eles podem ser executados em sua máquina doméstica, se você estiver apenas experimentando programação PHP. 

##### 3.1.2 Scripts de linha de comando
Você pode fazer um script PHP para executá-lo sem qualquer servidor ou navegador. Você só precisa do analisador PHP para usá-lo dessa forma. Este tipo de uso é ideal para scripts executados regularmente usando cron (no * nix ou Linux) ou Agendador de tarefas (no Windows). Esses scripts também podem ser usados ​​para tarefas simples de processamento de texto. 

PHP pode ser usado em todos os principais sistemas operacionais, incluindo Linux, muitas variantes do Unix (incluindo HP-UX, Solaris e OpenBSD), Microsoft Windows, macOS, RISC OS e provavelmente outros. O PHP também oferece suporte para a maioria dos servidores web hoje. Isso inclui Apache, IIS e muitos outros. E isso inclui qualquer servidor da web que pode utilizar o binário FastCGI PHP, como lighttpd e nginx. O PHP funciona como um módulo ou como um processador CGI.

Portanto, com o PHP, você tem a liberdade de escolher um sistema operacional e um servidor web. Além disso, você também tem a opção de usar a programação procedural ou a programação orientada a objetos (OOP), ou uma mistura de ambas.

#### 3.2 Geração e manipulação de arquivos
Com PHP, você não está limitado a gerar HTML, é possível a saída de imagens, arquivos PDF e até mesmo filmes Flash (usando libswf e Ming) gerados instantaneamente. Você também pode produzir facilmente qualquer texto, como XHTML e qualquer outro arquivo XML. O PHP pode gerar automaticamente esses arquivos e salvá-los no sistema de arquivos, em vez de imprimi-los, formando um cache do servidor para seu conteúdo dinâmico.

PHP tem recursos úteis de processamento de texto, que incluem expressões regulares compatíveis com Perl (PCR) e muitas extensões e ferramentas para analisar e acessar documentos XML. O PHP padroniza todas as extensões XML na base sólida de libxml2 e estende o conjunto de recursos adicionando suporte a SimpleXML, XMLReader e XMLWriter.

#### 3.3 Orientação a Objetos

#### 3.4 Uso de Banco de Dados
O PHP suporta uma ampla variedade de bancos de dados. O que torna a escrita de uma página da web habilitada para banco de dados simples, podendo utilizar uma das extensões específicas do banco de dados (por exemplo, para mysql), ou usando uma camada de abstração como PDO, ou conectando-se a qualquer banco de dados que suporte o padrão Open Database Connection por meio da extensão ODBC.

#### 3.5 Protocolos
PHP também possui suporte para comunicação com outros serviços usando protocolos como LDAP, IMAP, SNMP, NNTP, POP3, HTTP, COM (no Windows) entre outros.

### Referências
1. [Manual do PHP](https://www.php.net/manual/pt_BR/)
2. SEBESTA, Robert W. **Conceitos de linguagens de programação** [recurso eletrônico] /Robert W. Sebesta; tradução técnica: Eduardo Kessler Piveta. – 9. ed. – Dados eletrônicos. – Porto Alegre : Bookman, 2011.
3. [Wiki PHP](https://wiki.php.net/)
